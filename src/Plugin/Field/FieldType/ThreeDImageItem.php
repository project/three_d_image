<?php

namespace Drupal\three_d_image\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Defines the 'three_d_image_field_type' field type.
 *
 * @FieldType(
 *   id = "three_d_image_field_type",
 *   label = @Translation("3D Image"),
 *   category = @Translation("General"),
 *   default_widget = "three_d_image_field_widget",
 *   default_formatter = "three_d_image_field_formatter",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class ThreeDImageItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['depth_target_id'] = $properties['target_id'];
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['depth_target_id'] = [
      "description" => "The ID of the target entity.",
      "type" => "int",
      "unsigned" => TRUE,
    ];
    $schema['indexes']['depth_target_id'] = ['depth_target_id'];

    return $schema;
  }

}
